To compile and run on a Unix operating system:

Open terminal,


```
#!bash
cd /Path/To/Files
javac MainWindow.java
java MainWindow
```