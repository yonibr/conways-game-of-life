import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MainWindow extends JFrame
{
	public static final int SCREEN_WIDTH = 1500;
	public static final int SCREEN_HEIGHT = 1000;
	public static final int SCALE = 1; // Number of pixels per side of each cell
	public static final int BLACK = Color.black.getRGB();
	public static final int GREEN = Color.green.getRGB();
	public static final int NUMBER_OF_CELLS = 25000; // Reduce if increasing scale
	public static final int CALCULATION_DELAY = 3000; // Start calculating grid after 3 seconds
	
	static Grid grid = new Grid(SCREEN_WIDTH / SCALE, SCREEN_HEIGHT / SCALE, NUMBER_OF_CELLS);
	
	private static BufferedImage canvas;

	public MainWindow()
	{
		canvas = new BufferedImage(SCREEN_WIDTH, SCREEN_HEIGHT,
				BufferedImage.TYPE_INT_ARGB);
		setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
		setResizable(false);

		final JPanel panel = new JPanel()
		{
			@Override
			protected void paintComponent(Graphics g)
			{
				super.paintComponent(g);
				for (int x = 0; x < SCREEN_WIDTH / SCALE; x++)
					for (int y = 0; y < SCREEN_HEIGHT / SCALE; y++)
						for (int i = 0; i < SCALE; i++)
							for (int c = 0; c < SCALE; c++)
							{
								canvas.setRGB(x * SCALE + i, y * SCALE + c,
										grid.cells[x][y] ? GREEN : BLACK);
							}
				g.drawImage(canvas, 0, 0, null);
			}
		};

		panel.setBounds(getBounds());
		add(panel);
		new Timer().scheduleAtFixedRate(new TimerTask()
		{

			@Override
			public void run()
			{
				repaint();
			}
		}, 1000, 17);

	}

	public static void runLoop()
	{
		while (true)
		{
			if (canvas == null)
				continue;

			try
			{
				Thread.sleep(100);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			
			grid.calculate();
		}
	}

	public static void main(String[] args) throws InterruptedException
	{

		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				new MainWindow().setVisible(true);
			}
		});
		
		
		
		
		new Timer().schedule(new TimerTask()
		{

			@Override
			public void run()
			{
				runLoop();
			}
		}, CALCULATION_DELAY);

	}

}
