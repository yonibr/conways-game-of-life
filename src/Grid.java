import java.util.Random;

import org.w3c.dom.css.Counter;

public class Grid
{
	public boolean[][] cells;

	public Grid(int width, int height)
	{
		Random random = new Random();
		cells = new boolean[width][height];

		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++)
				cells[x][y] = random.nextBoolean();
	}
	
	public Grid(int width, int height, int numCells)
	{
		Random random = new Random();
		cells = new boolean[width][height];
		
		for (int i = 0; i < numCells; i++)
			cells[random.nextInt(width)][random.nextInt(height)] = true;
	}

	void calculate()
	{
		for (int x = 0; x < cells.length; x++)
			for (int y = 0; y < cells[0].length; y++)
			{
				int neighbors = 0;
				int counter = 0;
				for (int i = -1; i < 2; i++)
					for (int c = -1; c < 2; c++)
					{
						if ((x == 0 && i == -1) || (x == cells.length - 1 && i == 1))
							continue;
						if ((y == 0 && c == -1) || (y == cells[0].length - 1 && c == 1))
							continue;
						if (i == 0 && c == 0)
							continue;
						
						neighbors += cells[x + i][y + c] ? 1 : 0;
					}
				

				if (cells[x][y] == true && (neighbors > 3 || neighbors < 2))
					cells[x][y] = false;
				else if (cells[x][y] == false && neighbors == 3)
					cells[x][y] = true;
			}
	}
}
